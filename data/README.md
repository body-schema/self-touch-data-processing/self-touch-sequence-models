
Data
=======

# Description
The used data format in this repository is the time series representation of manually coded qualitative data.

Content:

+ **docs**: contains documentation for command line tools `convert2tm`, `convert2tm_all`
+ **SELF_TOUCH_TIMESERIES**: (dummy) contains only data from a few recording for examples; replace with SELF_TOUCH_TIMESERIES database
+ **other_data** contains _connectivity\_table.csv_: mask of connected areas
+ **visualization_data**: contains data for plotting transitions on infant body scheme: image of body scheme; csv file with areas coordinates in image. 

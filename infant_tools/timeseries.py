import os
import pandas as pd
import numpy as np
from copy import deepcopy

# contains function for working with time series

RH = "RH"
LH = "LH"


def get_actions_timeseries(timeseries):
    """creates array of actions in chronological order from timeseries
        each touch or NO touch will be represented only by one entry in the actions series 
        e.g. [...,UPPER, UPPER, UPPER, NONE, NONE, NONE, HEAD HEAD, ...] -> [..., UPPER, NONE, HEAD, ...]

    Args:
        timeseries (numpy.ndarray): _description_

    Returns:
        numpy.ndarray: actions time series
    """
    actions = [timeseries[0]] +  [timeseries[i] for i in range(1, len(timeseries)) if timeseries[i-1] != timeseries[i]]
    actions = np.array(actions)
    return actions 


def split_timeseries_by_help(tm, help_symbol):
    """time series is split into parts 'before/after' help (during recording)

    Args:
        tm (numpy.ndarray): time series 
        help_symbol (str): label of help (default "HELP") in time series

    Returns:
        list: list of numpy.ndarrays as time series before, between, after help
    """
    idx = np.where(tm!=help_symbol)[0]
    tm_parts = np.split(tm[idx],np.where(np.diff(idx)!=1)[0]+1)
    return tm_parts


def back2normal(tm):
    """converts 'back' areas in time series to front areas
        e.g., 17LB to 17L 

    Args:
        tm (numpy.ndarray): time series

    Returns:
        numpy.ndarray: time series with back areas converted to front areas
    """
    _back2normal = lambda x: x.replace("B", "") if "B" in x else x
    return np.array([_back2normal(v) for v in tm])


def get_data_sequences(series, order=1):
    """creates list from moving window of size "order + 1"
        e.g. [NONE, UPPER, NONE, LOWER] -> [[NONE, UPPER], [UPPER, NONE], [NONE, LOWER]]


    Args:
        series (numpy.ndarray): numerical time series or actions time series
        order (int): size of moving window + 1. Defaults to 1.

    Returns:
        numpy.ndarray: list of all transitions 
    """
    trans_data = []
    for i in range(order, len(series)):
        trans_data.append(series[i-order:i + 1])
        
    return np.array(trans_data)


def sliding_window(time_series, window_size, function, *args, **kwargs):
    """slide window of size window_size by one and apply function on every iteration 

    Args:
        time_series (numpy.ndarray): -
        window_size (int): size of sliding window 
        function (Callable): function to be applied on every window

    Returns:
        numpy.ndarray: results of function for every window step
    """
    tm = []
    assert window_size <= len(time_series)
    for i in range(0, len(time_series) - window_size):
        tm.append(function(time_series[i:i+window_size], *args, **kwargs))
    return tm


def count_pattern(time_series, seq):
    """count number of sequences in time series

    Args:
        time_series (numpy.ndarray): time series or series of actions ,...
        seq (list): sequence to be counted

    Returns:
        int: number of sequences in data
    """
    idx = np.arange(0, len(time_series))
    for i, val in enumerate(seq):
        idx = idx[np.where(time_series[idx] == val)]
        print(time_series[idx])
        idx+=1
    return len(idx)


def merge_locations(time_series, locations_dict):
    """replace locations (states) in time series with other location defined by locations_dict, 
    could be used to merge multiple locations together (simplification of time series, cluster representation of t.m.)

    Args:
        time_series (numpy.ndarray): time series representation of qualitative data
        locations_dict (dict): dictionary {cluster_1: [location_1, location_2, ...], cluster_2 : [...]...} 

    Returns:
        numpy.ndarray: time series where locations corresponding to clusters are replaced by cluster names
    """
    time_series = deepcopy(time_series)
    for cluster in locations_dict.keys():
        for location in locations_dict[cluster]:
            time_series[np.where(time_series == location)] = cluster
    return time_series


def get_symbolic_timeseries(table):
    """creates symbolic representation of time series for each hand
        each element of the time series represents a frame of the recording
        and has its value is string representation of touched location (a columns names of the table)
        if no touch took place, corresponding value is {hand}_NONE

    Args:
        table (pandas DataFrame): loaded spatiotemporal.csv

    Returns:
        numpy.ndarray: array symbolic time series for each hand
    """
    RH_timeseries = np.repeat("RH_NONE", table.shape[0]).astype("U12")
    LH_timeseries = np.repeat("LH_NONE", table.shape[0]).astype("U12")
    for col in table:
        if RH in col:
            RH_timeseries[table[col] == 1] = col
        elif LH in col:
            LH_timeseries[table[col] == 1] = col
    return RH_timeseries, LH_timeseries


def get_mapping(table):
    """for faster working with time series
        string state values are mapped to numbers
        should be generated only once for dataset

    Args:
        table (pandas.DataFrame): any loaded spatiotemporal.csv file from the set, we are working on

    Returns:
        dict: key is the symbolic (string) name of the state (as col. names in .csv), value is assigned number
    """
    columns = list(set([name.split("_", 1)[-1] for name in table.columns]))
    mapping = {key: value for key, value in zip(columns, [i for i in range(1,len(columns)+1)])}
    mapping["NONE"] = 0
    return mapping 


def get_timeseries(table, hand, mapping):
    """get representation of timeseries coded to numbers
        see functions get_symbolic_timeseries, get_mapping

    Args:
        table (pandas.DataFrame): loaded spatiotemporal.csv
        hand (string): "RH" or "LH"
        mapping (dict): result of func. get_mapping (only one per dataset)

    Returns:
        numpy.ndarray: time series where each element corresponds to frame and it's value is defined by mapping
    """
    timeseries = np.repeat(0, table.shape[0])
    for col in table:
        if hand in col:
            timeseries[table[col] == 1] = mapping[col.split("_", 1)[-1]]
    return timeseries


def num_tm2str_tm(timeseries, mapping):
    """convert time series (from get_timeseries) to symbolic / string representation (as get_symbolic_timeseries)

    Args:
        timeseries (numpy.ndarray): numerical time series, (each number represents state defined in mapping) 
        mapping (dict): result of func. get_mapping (only one per dataset)
    
    Returns:
        numpy.ndarray: symbolic representation of time series
    """
    
    inv_map = {v: k for k, v in mapping.items()}
    sym_tm = []
    for val in timeseries:
        sym_tm.append(inv_map[val])
    return np.array(sym_tm).astype("U12")


def remove_no_touch(timeseries, none_state):
    """removes NONE state (elements of time series when infant is not touching)

    Args:
        timeseries (numpy.ndarray): time series 
        none_state (string): value representing No Touch in time series

    Returns:
        numpy.ndarray, dict: time series without NONE touch state
    """
    return np.delete(timeseries, np.where(timeseries == none_state))


def get_actions_count(timeseries, none_state):
    """ returns an array containing the durations touches in time series
        the i-th element of returned array corresponds to the duration (in frames) of the i-th action.
        actions corresponds to the touch area from actions time series

    Args:
        timeseries (numpy.ndarray): time series 
        none_state (string): value representing No Touch in time series

    Returns:
        numpy.ndarray: count of frames per actions (touches)
    """
    indices = np.where(timeseries[:-1] != timeseries[1:])[0] + 1
    # Count the number of repetitions
    counts_states = np.diff(np.concatenate(([0], indices, [len(timeseries)])))
    idx = 0
    touches_count = []
    for i in range(len(counts_states)):
        state = timeseries[idx + counts_states[i] - 1]
        if state != none_state:
            touches_count.append(counts_states[i])
        idx += counts_states[i]
    return np.array(touches_count)


def get_pauses_between_touches(timeseries, none_state):
    """returns an array containing the duration of no touch between touched areas.
        the i-th element corresponds to the duration (in frames) of pause between i-th and (i+1)-th action.
        actions corresponds to the touch area from actions time series.
        the pause for complex touch is 0

    Args:
        timeseries (numpy.ndarray): time series 
        none_state (string): value representing No Touch in time series

    Returns:
        numpy.ndarray: count of no touch frames between actions (touches)
    """
    actions = get_actions_timeseries(timeseries)
    
    indices = np.where(timeseries[:-1] != timeseries[1:])[0] + 1
    # Count the number of repetitions
    counts_states = np.diff(np.concatenate(([0], indices, [len(timeseries)])))
    counts = []
    for i in range(len(actions)-1):
        if actions[i] == none_state:
            counts.append(counts_states[i])
        elif actions[i] != none_state and actions[i+1] != none_state:
            counts.append(0)
            
    if actions[-1] == none_state:
        counts.append(counts_states[-1])

    if actions[0] == none_state:
        counts = counts[1:]
    if actions[-1] == none_state:
        counts = counts[:-1]
    return counts

import os
import cv2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

from .load_data import apply_data_dict_function


RH = "RH"
LH = "LH"
hands = [RH, LH]

infants = ["AA", "TH", "CA", "RT"]
infant_colors = ["#9A32CD", "#FF7F50", "#FF1493", "#00BFFF"]



def _get_x(filename):
    """from filename returns age as week + day/7

    Args:
        filename (str): name of recording

    Returns:
        int: infant age for recording
    """
    get_day = lambda filename: int(filename.split("w-")[-1][0])
    get_week = lambda filename: int(filename.split("w-")[0].split("_")[-1])
    return get_week(filename) + get_day(filename)/7


def plot_data_dict(data_dict, filename_dict, y_label, ax=None, **kwargs):
    """plot data_dict values in one figure, y-axis: values in data_dict, x-axis: age

    Args:
        data_dict (dict): data dictionary with structure {infant:{week:[values]}} 
        filename_dict (dict): recordings names with structure {infant:{week:[recordings names]}}
        y_label (str): y label for figure
        ax (matplotlib.axes.Axes, optional): axis to plot on. Defaults to None - new figure is created.

    Returns:
        matplotlib.axes.Axes: ax with plotted data_dict values 
    """

    if ax is None:
        fig, ax = plt.subplots(1, 1)

    time_vals = apply_data_dict_function(filename_dict, _get_x)

    for inf_idx, infant in enumerate(data_dict.keys()):
        x = []
        y = []
        for week_idx, week in enumerate(data_dict[infant].keys()):
            x += time_vals[infant][week]
            y +=  data_dict[infant][week]        

        ax.plot(x, y, label = list(data_dict.keys())[inf_idx], **kwargs)
        
    ax.set_xlabel("age [weeks]")
    ax.set_ylabel(y_label)
    ax.legend()
    return ax


def _sem(values):
    """computes standard error of mean

    Args:
        values (array like): values to compute from

    Returns:
        float: standard error of mean
    """
    values = np.array(values)
    #calculate standard error of the mean 
    return np.std(values, ddof=1) / np.sqrt(np.size(values))


def plot_data_dict_sem(data_dict, y_label, ax=None, errorbar=True, alpha=0.4, **kwargs):
    """ plot data_dict values in one figure, y-axis: values with SEM (standard error of mean) in data_dict, x-axis: age (weeks)
        SEM is computed from values in week level : {infant:{week:[values]}}

    Args:
        data_dict (dict): data dictionary with structure {infant:{week:[values]}} 
        filename_dict (dict): recordings names with structure {infant:{week:[recordings names]}}
        y_label (str): y label for figure
        ax (matplotlib.axes.Axes, optional): axis to plot on. Defaults to None - new figure is created.
        errorbar (bool, optional): enable to plot errorbar. Defaults to True. If False, SEM is plotted as shaded area.


    Returns:
        matplotlib.axes.Axes: ax with plotted data_dict values 
    """
    if ax is None:
        fig, ax = plt.subplots(1, 1)


    for inf_idx, infant in enumerate(data_dict.keys()):
        x = []
        y = []
        e = []
        for week_idx, week in enumerate(data_dict[infant].keys()):
            x.append(int(week[:-1]))
            y.append(np.mean(data_dict[infant][week]))
            e.append(_sem(data_dict[infant][week]))    

        y = np.array(y)
        e = np.array(e)
        x = np.array(x)
        if errorbar:
            plt.errorbar(x, y, e, label = list(data_dict.keys())[inf_idx], fmt='-o', **kwargs)
        else:
            ax.plot(x, y, label = list(data_dict.keys())[inf_idx], **kwargs)
            ax.fill_between(x, y-e, y+e, alpha=alpha)

    ax.set_xlabel("age [weeks]")
    ax.set_ylabel(y_label)
    ax.legend()
    return ax

def draw_hitmap(data, x, y, locs, images_path, LOCATIONS, max_val=1):
    """plot hitmap type of infants body from body parts; TODO: will be added 

    Args:
        data (_type_): _description_
        x (_type_): _description_
        y (_type_): _description_
        locs (_type_): _description_
        images_path (_type_): _description_
        LOCATIONS (_type_): _description_
        max_val (int, optional): _description_. Defaults to 1.

    Returns:
        _type_: _description_
    """
    grid_size = 1150
    final_image_rgb = np.zeros((grid_size*2, grid_size, 3))

    cmap = plt.colormaps["Reds"]
    norm = Normalize(vmin=0, vmax=max_val) # TODO 0 to 1 or real max

    for i in range(len(locs)):
        image = cv2.imread(os.path.join(images_path, '{}.png'.format(locs[i])), cv2.IMREAD_GRAYSCALE)
        if image is None:
            print("missing ", locs[i])
            continue
        edges = cv2.Canny(image, 128, 255, cv2.THRESH_BINARY)
        contours, _ = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cv2.drawContours(image, contours, -1, (255, 255, 255), cv2.FILLED)
        new_img = np.zeros((image.shape[0], image.shape[1], 3))
        # get color value
        loc = locs[i][1:] + locs[i][0] # R17 -> 17R
        # names of body parts are flipped
        if "R" in loc:
            loc = loc.replace("R", "L")
        else:
            loc = loc.replace("L", "R")
        idx = LOCATIONS.index(loc)
        new_img[np.where(image == 255)] = cmap(norm(data[idx]))[:-1]
        (h, w, _) = new_img.shape
        final_image_rgb[y[i]:y[i]+h, x[i]:x[i]+w, :] = final_image_rgb[y[i]:y[i]+h, x[i]:x[i]+w, :] + new_img #TODO: check if not overwriting
    return final_image_rgb
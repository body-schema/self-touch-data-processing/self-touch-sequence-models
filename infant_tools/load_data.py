import os
import pandas as pd
import numpy as np

filename = "spatiotemporal.csv"

INFANTS = ["AA", "TH", "CA", "RT"]


def get_all_data(data_path):
    """create a dictionary with structure same as database (time series representation) 
        {INFANT: {week: [recording1, recording2, ...]}}
        values are pandas.DataFrame loaded time series

    Args:
        data_path (str): path to database folder (infant/week/recording/spatiotemporal.csv), files need to be names: spatiotemporal.csv

    Returns:
        dict: data_dict: loaded time series as pandas.DataFrame; filename_dict: same structure with recordings names
    """
    
    data_dict = dict()
    filename_dict = dict()
    for infant in INFANTS:
        infant_data_dict = {}
        infant_filename_dict = {}
        weeks = os.listdir(os.path.join(data_path, infant))
        weeks = [week for week in weeks if os.path.isdir(os.path.join(data_path, infant, week))]
        weeks = sorted(weeks, key=lambda x: int(x[:-1]))
        for week in weeks:
            data = []
            recordings_true = []
            recordings = os.listdir(os.path.join(data_path, infant, week))
            for rec in recordings:
                file_path = os.path.join(data_path, infant ,week, rec, filename)
                if os.path.isfile(file_path):
                    data.append(pd.read_csv(file_path, sep="\t"))
                    recordings_true.append(rec)
            if data:
                infant_data_dict[week] = data
                infant_filename_dict[week] = recordings_true
        data_dict[infant] = infant_data_dict
        filename_dict[infant] = infant_filename_dict

    return data_dict, filename_dict


def save_data(data_path, data_dict, filename_dict):
    """saves data_dict with names from filename_dict to data_path

    Args:
        data_path (str): path where saved data will be stored
        data_dict (dict):data dictionary to save
        filename_dict (dict): dictionary with recordings names
    """
    for infant in filename_dict.keys():
        inf_folder = os.path.join(data_path, infant)
        if not os.path.isdir(inf_folder):
            os.mkdir(inf_folder)
        for week in filename_dict[infant].keys():
            w_folder = os.path.join(inf_folder, week)
            if not os.path.isdir(w_folder):
                os.mkdir(w_folder)
            for i, file in enumerate(filename_dict[infant][week]):
                path_file = os.path.join(w_folder, file + ".csv")
                data_dict[infant][week][i].to_csv(path_file, sep="\t", index=False)
            

def get_video_length(video_file, framerate_file):
    """returns duration of recording in seconds

    Args:
        video_file (str): recording name
        framerate_file (str): path to framerate.csv

    Raises:
        FileNotFoundError: if recording is not framerate.csv (recommend checking for typos, there used to be some in framerate.csv)

    Returns:
        float: recording duration
    """
    try:
        framerate = pd.read_csv(framerate_file, delimiter=",")
        length = float(framerate.loc[:, "durations"][framerate["FileName"] == video_file])
        return length / 1000
    except: raise FileNotFoundError(video_file)


def get_video_framerate(video_file, framerate_file):
    """returns framerate of video recording 

    Args:
        video_file (str): recording name
        framerate_file (str): path to framerate.csv

    Raises:
        FileNotFoundError: if recording is not framerate.csv (recommend checking for typos, there used to be some in framerate.csv)

    Returns:
        float: framerate of recording
    """
    try:
        framerate_file = pd.read_csv(framerate_file, delimiter=",")
        framerate = float(framerate_file.loc[:, "framerates"][framerate_file["FileName"] == video_file])
        return framerate
    except: raise FileNotFoundError(video_file)



def apply_data_dict_function(data_dict, function, *args, **kwargs):
    """ apply function to each value in data_dict 
        and returns a new data dict with the results  

    Args:  
        data_dict (dict): data in dictionary format (infant:week:[recordings])
        function (Callable): function to process values in data_dict with at least one argument

    Returns:
        dict: new data_dict with function values 
    """
    # run function on every table in data dict and return same structure dict with results
    opt_dict = "filename_dict" # optional values from same structure dictionary to pass to function
    filename_dict = kwargs.get(opt_dict, None)
    if filename_dict:
        kwargs.pop(opt_dict)

    new_dict = create_empty_dataset_dict(data_dict)
    for infant in list(data_dict.keys()):
        # new_dict[infant] = {}
        weeks = list(data_dict[infant].keys())
        for week in weeks:
            # new_dict[infant][week] = []
            for i, data in enumerate(data_dict[infant][week]):
                if filename_dict:
                    new_dict[infant][week].append(function(data, filename_dict[infant][week][i], *args, **kwargs))
                else:
                    new_dict[infant][week].append(function(data, *args, **kwargs))
    return new_dict


def create_empty_dataset_dict(copy_dict):
    """creates empty dictionary with same structure as argument

    Args:
        copy_dict (dict): dictionary to copy

    Returns:
        dict: empty dictionary
    """
    new_dict = {}
    for infant in list(copy_dict.keys()):
        new_dict[infant] = {}
        weeks = list(copy_dict[infant].keys())
        for week in weeks:
            new_dict[infant][week] = []
    return new_dict


def filter_data(data_dict1, data_dict2, function, threshold, *args, **kwargs):
    """apply function to each value in data_dict1 and returns a new data_dict1 and data_dict2
        with entries for which function output was >= than threshold
        also returns dictionary with the output values (same structure as data_dict) 
        e.g., good for filtering short recordings        

    Args:
        data_dict1 (dict): data in dictionary format (infant:week:[recordings]), function is applied to this dictionary
        data_dict2 (dict): data in dictionary format (infant:week:[recordings])
        function (Callable): function to process values in data_dict1 with at least one argument
        threshold (dict): only entries with function output >= threshold are kept 

    Returns:
        dict: new_data_dict1: filtered data_dict1, new_data_dict2: data_dict2 with the same structure as data_dict1, new_val_dict: values from function
    """
    func_dict = apply_data_dict_function(data_dict1, function, *args, **kwargs)
    new_val_dict = create_empty_dataset_dict(data_dict1) # values from function > threshold
    new_data_dict1 = create_empty_dataset_dict(data_dict1) # filtered data
    new_data_dict2 = create_empty_dataset_dict(data_dict2) # filtered filename dict
    for infant in func_dict:
        for week in func_dict[infant]:
            for i, val in enumerate(func_dict[infant][week]):
                if val >= threshold:
                    new_val_dict[infant][week].append(val)
                    new_data_dict1[infant][week].append(data_dict1[infant][week][i])
                    new_data_dict2[infant][week].append(data_dict2[infant][week][i])
            # remove empty
            if not new_data_dict1[infant][week]:
                del new_val_dict[infant][week]
                del new_data_dict1[infant][week]
                del new_data_dict2[infant][week]
        # delete infant if empty
        if not new_data_dict1[infant]:
            del new_data_dict1[infant]
            del new_val_dict[infant]
            del new_data_dict2[infant]
    return new_data_dict1, new_data_dict2, new_val_dict


def apply_data_dict_function_all_weeks(data_dict, function, *args, **kwargs):
    """same as apply_data_dict_function, but the user function is applied to the whole week values 

    Args:
        data_dict (dict): data in dictionary format (infant:week:[recordings])
        function (Callable): function to process week values in data_dict with at least one argument expecting list with values

    Returns:
        dict: dictionary with format {infant:week:[function output]}
    """

    new_dict = create_empty_dataset_dict(data_dict)
    for infant in list(data_dict.keys()):
        weeks = list(data_dict[infant].keys())
        for week in weeks:
            new_dict[infant][week].append(function(data_dict[infant][week], *args, **kwargs))
    return new_dict


def average_data_dict(data_dict):
    """takes values from data_dict in infant's week and return average 

    Args:
        data_dict (dict): data_dict in format data_dict[infant][week] = list

    Returns:
        dict: average dict for each week
    """
    new_dict = create_empty_dataset_dict(data_dict)
    for infant in data_dict.keys():
        for week in data_dict[infant].keys():
            new_dict[infant][week] = sum(data_dict[infant][week]) / len(data_dict[infant][week])
    return new_dict


def join_data_dict(data_dict1, data_dict2):
    """joins two data dicts 

    Args:
        data_dict1 (dict): dict 1
        data_dict2 (dict): dict 2

    Returns:
        dict:joined data_dict 
    """
    new_dict = create_empty_dataset_dict(data_dict1)
    for infant in data_dict1.keys():
        for week in data_dict1[infant].keys():
            new_dict[infant][week] = data_dict1[infant][week] + data_dict2[infant][week]
    return new_dict

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
import networkx as nx
import igraph as ig

from itertools import product
from .timeseries import get_actions_timeseries, get_pauses_between_touches


class Transitions():
    """object for counting transitions frequencies,
        conversion of frequency matrix to graph representation, 
        plotting of frequency matrix, 
        estimating markov model """

    def __init__(self, states, order=1):
        """Transitions class

        Args:
            states (list): list of states for model
            order (int, optional): define order of classical markov model. Defaults to 1.
        """        
        self.order = order
        self.lag = order
        self.labels = {i:states[i] for i in range(len(states))}
        self.labels_inv = {states[i]:i for i in range(len(states))}
        self.size = len(states)
        # labels for rows of tables
        if order > 1:
            self.ylabels = list(product(states, repeat = self.order))
        
        self.transition_matrix = np.zeros((self.size**self.order, self.size)) # transitions probabilities
        self.frequency_matrix = np.zeros((self.size**self.order, self.size)) # transitions frequencies

        # for estimating markov model
        self.laplace_smoothing_coef = 0.1


    def sequencer(self, tm):
        """generate sequences from time series for learning the model

        Args:
            tm (numpy.ndarray): time series (represented by sequence of states)

        Returns:
            x,y (numpy.ndarray): x represents series of previous states whereas y represents the next states
        """
        x = tm[:-1]
        y = tm[self.lag:]

        # group if higher order
        if self.order > 1:
            x = [x[i:i+self.order] for i in range(len(x)-self.order+1)]

        x = np.array(x)
        y = np.array(y)
        return x,y
    

    def symbol_to_num(self, tm):
        """converts symbolic time series to numeric (for easier computation)

        Args:
            tm (numpy.ndarray): time series (represented by sequence of states)

        Returns:
            numpy.ndarray: converted time series
        """
        tm = np.array([self.labels_inv[val] for val in tm])
        return tm


    def fit_markov_model(self, x, y):
        """computes transition matrix from sequenced time series data,
            works also with order > 1

        Args:
            x (numpy.ndarray): x from sequencer
            y (numpy.ndarray): y from sequencer

        Returns:
            numpy.ndarray: transition matrix (value in row r and column c is the probability of transition from state r to state c)
                        order of these states is the same as in states when defining the model
        """
        # generate high order states
        hstates = np.array(list(product(list(self.labels.keys()), repeat = self.order)))

        # iterate over states and compute probability
        for i in range(self.size**self.order):
            if self.order > 1:
                count_next = np.bincount(y[np.where(np.all(x == hstates[i], axis=1))[0]], minlength=self.size)
            else:
                count_next = np.bincount(y[np.where(x == i)[0]], minlength=self.size)
            count_sum = np.sum(count_next)
            if count_sum == 0:
                count_sum = 1
            elif count_sum == 0:
                count_next = np.ones((1, self.size))
                count_sum = self.size
            self.transition_matrix[i, :] = count_next/count_sum
        return self.transition_matrix
    
    
    def count_transitions_in_range(self, tm, none_state, none_range, exclude_complex_touch=False):
        """count frequency of transitions between touched areas (from time series)
            only transitions between touched ares within `none_range` interval are included 

        Args:
            tm (numpy.ndarray): time series 
            none_state (str): defined label of no touch state in time series
            none_range (list): [min, max] range in number of frames 
            exclude_complex_touch (bool, optional): set to True if you want to count only transitions between simple touches 
                (e.g., interval is [1 s, 10 s] and actions are: '5L', 'NONE', '7L', '6L', 'NONE', '4L', 'NONE', '5L' then
                if duration of 'NONE' in in the interval the counted transitions are only 4L->5L;
                if set to False counted transitions are: 5L->7L, 6L->4L, 4L->5L).
                should be False when interval is [0s, ..s] to count transitions within complex touch. Defaults to False.

        Raises:
            ValueError: if this object is defined for order > 1

        Returns:
            numpy.ndarray: frequency matrix
        """
        
        # works only for order = 1
        if self.order >1:
            raise ValueError("works only for order 1")
        
        actions = get_actions_timeseries(tm)
        none_count = get_pauses_between_touches(tm, none_state)
        
        actions = self.symbol_to_num(actions)
        none_state = self.labels_inv[none_state]

        actions = [val for val in actions if val != none_state]

        frequencies = np.zeros((self.size, self.size))

        none_idx = 0
        for i in range(len(actions) - 1):
            val = 0
            if none_count[none_idx] >= none_range[0] and none_count[none_idx] <= none_range[1]:
                # val = max([0, 1 - none_count[none_idx]/no_touch_threshold_frames])
                val = 1
            if i > 0 and exclude_complex_touch:
                if none_count[none_idx-1] == 0:
                    val = 0
            if i < len(none_count)-1 and exclude_complex_touch:
                if none_count[none_idx+1] == 0:
                    val = 0
            none_idx += 1
            frequencies[actions[i], actions[i+1]] += val
        
        # delete none state from transition matrix
        frequencies = np.delete(frequencies, (none_state), axis=1)
        frequencies = np.delete(frequencies, (none_state), axis=0)
        self.size -= 1
        del self.labels_inv[self.labels[none_state]]
        del self.labels[none_state]
        self.frequency_matrix = frequencies
        return frequencies


    def normalize(self, frequency_matrix=None, laplace_smoothing=False):
        """normalize rows of frequency matrix. the result is a transition matrix
            with transition probabilities from row areas to column area  

        Args:
            frequency_matrix (numpy.ndarray, optional): matrix to normalize. Defaults to None - self.frequency_matrix is used.
            laplace_smoothing (bool, optional): apply laplace smoothing. Defaults to False.

        Returns:
            numpy.ndarray: transition matrix
        """
        if not np.any(frequency_matrix):
            frequency_matrix = self.frequency_matrix

            # normalize
        transition_matrix = np.zeros(frequency_matrix.shape)
        for row in range(self.size):
            count = np.sum(frequency_matrix[row,:])
            if count == 0:
                transition_matrix[row,:] = np.ones((1, self.size))
                count = self.size
            else:
                transition_matrix[row, :] = frequency_matrix[row, :]

            if laplace_smoothing:
                transition_matrix[row,:] = transition_matrix[row,:] + self.laplace_smoothing_coef
                count += self.size*self.laplace_smoothing_coef 

            transition_matrix[row,:] = transition_matrix[row,:] / count
        self.transition_matrix = transition_matrix
        return transition_matrix


    def pruning(self, cutoff, frequency_matrix=None):
        """ remove areas touched less then cutoff*100 % 

        Args:
            cutoff (int): cutoff*100 % is the threshold 
            frequency_matrix (numpy.ndarray, optional): matrix to apply this function. 
                    Defaults to None - self.frequency_matrix is used.

        Returns:
            numpy.ndarray: pruned frequency matrix 
        """
        if not np.any(frequency_matrix):
            frequency_matrix = self.frequency_matrix
        if np.max(frequency_matrix) <= 1:
            print("does not look like frequency matrix")
        
        # frequency_matrix[frequency_matrix <= cutoff] = 0
        frequency_matrix[frequency_matrix.sum(1)/frequency_matrix.sum() <= cutoff,:] = 0

        return frequency_matrix


    def plot(self, ax, cbar_ax=None):
        """plot frequency matrix on axis
            only functional if order = 1

        Args:
            ax (matplotlib axis): axis to be plot on
            cbar_ax (matplotlib axis, optional): axis for color bar. Defaults to None.

        Returns:
            matplotlib axis: axis with plotted transition matrix
        """
        if self.order == 1: 
            cbar=False
            if cbar_ax:
                cbar=True
            ax = sns.heatmap(self.frequency_matrix, ax=ax,
                cbar=cbar,
                vmin=0, vmax=np.max(self.frequency_matrix),
                cmap='Reds',
                linewidth=.5,
                cbar_ax= cbar_ax,
                yticklabels = self.labels.values(),
                xticklabels = self.labels.values())
            return ax
        return None
    
    
    def get_transition_table(self):
        """ returns transition table as pandas dataframe
            useful for "visualization" of higher order models (use of multiindex) 

        Returns:
           pandas.DataFrame: table representing transition probabilities between touched areas
        """
        labels_y = [tuple(i) for i in self.ylabels]
        multiindex  = pd.MultiIndex.from_tuples(labels_y)
        table = pd.DataFrame(self.transition_matrix, columns = list(self.labels.values()), index=multiindex)
        return table
    

    def create_graph(self, matrix=None, threshold=0, max_count=None):
        """creates a weighted directed graph representation of frequency matrix
            using networkx library 

        Args:
            matrix (numpy.ndarray, optional): matrix to normalize. Defaults to None - self.frequency_matrix is used.
            threshold (int, optional): only transitions with weight (frequency) > threshold are included. Defaults to 0.
            max_count (int, optional): include only the max_count number of most frequent transitions. 
                                    Defaults to None - all transitions are included.

        Returns:
            networkx.classes.digraph.DiGraph: weighted directed graph representation of frequency matrix
        """
        if matrix is None:
            frequency_matrix = self.frequency_matrix
        else:
            frequency_matrix = matrix
        # create weighted directed graph from edges
        self.DG = nx.DiGraph()
        count = 0
        if max_count is None:
            max_count = len(frequency_matrix)* len(frequency_matrix)

        for i in range(len(frequency_matrix)):
            for j in range(len(frequency_matrix[i])):
                if frequency_matrix[i][j] > threshold and count < max_count:
                    self.DG.add_edge(i, j, weight=frequency_matrix[i][j])
        # create weighted directed graph
        nx.set_node_attributes(self.DG, self.labels, "labels")

        if len(self.DG.nodes) < 1:
            return False 
        return self.DG

        
    def draw_graph(self, ax, positions, DG=None, edge_cmap=plt.cm.Blues, **kwargs):
        """draw directed graph on axis

        Args:
            ax (matplotlib.axes.Axes): axes to plot on
            positions (dict): positions of labels 
            DG (networkx.classes.digraph.DiGraph, optional): Directed weighted graph to plot. Defaults to None - self.DG is used.
            edge_cmap (matplotlib.colors.Colormap, optional): Colormap used for transitions. Defaults to plt.cm.Blues.
        """
        ## CHANGE IN nx_pylab.py line 786 to increase self loop size
        if not DG:
            DG = self.DG
            # nodes_sizes_vals = self._get_node_sizes(DG=DG, **kwargs)
        _, weights = zip(*nx.get_edge_attributes(DG, 'weight').items())

        nx.draw(DG, pos=positions , ax=ax, edge_color=weights, edge_cmap=edge_cmap, **kwargs) 
        
    
    # def __call__(self, tm, save_counts=False):
    #     """ converts data to numeric representation, creates sequences,
    #       and computes transition matrix

    #     Args:
    #         tm (numpy.ndarray): time series (represented by sequence of states)

    #     Returns:
    #        numpy.ndarray: transition matrix of model
    #     """
    #     if save_counts:
    #         self.unique_states, self.states_counts = np.unique(tm, return_counts=True)
    #     tm = self.symbol_to_num(tm)
    #     x,y = self.sequencer(tm)
    #     return self.fit_markov_model(x,y)
    

    # def _get_node_sizes(self, DG=None, offset=5, gain=50):
    #     if not DG:
    #         DG = self.DG
    #     nodes_sizes_unordered = offset + np.ceil(gain*self.states_counts/np.max(self.states_counts)) # normalize

    #     nodes_map = [x[1] for x in nx.get_node_attributes(DG,'labels').items()]
    #     nodes_sizes = []
    #     for node in nodes_map:
    #         nodes_sizes.append(nodes_sizes_unordered[np.where(self.unique_states == node)][0])
    #     return nodes_sizes
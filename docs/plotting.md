<!-- markdownlint-disable -->

<a href="../infant_tools/plotting.py#L0"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

# <kbd>module</kbd> `plotting`




**Global Variables**
---------------
- **RH**
- **LH**
- **hands**
- **infants**
- **infant_colors**

---

<a href="../infant_tools/plotting.py#L34"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `plot_data_dict`

```python
plot_data_dict(data_dict, filename_dict, y_label, ax=None, **kwargs)
```

plot data_dict values in one figure, y-axis: values in data_dict, x-axis: age 



**Args:**
 
 - <b>`data_dict`</b> (dict):  data dictionary with structure {infant:{week:[values]}}  
 - <b>`filename_dict`</b> (dict):  recordings names with structure {infant:{week:[recordings names]}} 
 - <b>`y_label`</b> (str):  y label for figure 
 - <b>`ax`</b> (matplotlib.axes.Axes, optional):  axis to plot on. Defaults to None - new figure is created. 



**Returns:**
 
 - <b>`matplotlib.axes.Axes`</b>:  ax with plotted data_dict values  


---

<a href="../infant_tools/plotting.py#L81"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `plot_data_dict_sem`

```python
plot_data_dict_sem(data_dict, y_label, ax=None, errorbar=True, **kwargs)
```

plot data_dict values in one figure, y-axis: values with SEM (standard error of mean) in data_dict, x-axis: age (weeks)  SEM is computed from values in week level : {infant:{week:[values]}} 



**Args:**
 
 - <b>`data_dict`</b> (dict):  data dictionary with structure {infant:{week:[values]}}  
 - <b>`filename_dict`</b> (dict):  recordings names with structure {infant:{week:[recordings names]}} 
 - <b>`y_label`</b> (str):  y label for figure 
 - <b>`ax`</b> (matplotlib.axes.Axes, optional):  axis to plot on. Defaults to None - new figure is created. 
 - <b>`errorbar`</b> (bool, optional):  enable to plot errorbar. Defaults to True. If False, SEM is plotted as shaded area. 





**Returns:**
 
 - <b>`matplotlib.axes.Axes`</b>:  ax with plotted data_dict values  


---

<a href="../infant_tools/plotting.py#L123"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `draw_hitmap`

```python
draw_hitmap(data, x, y, locs, images_path, LOCATIONS, max_val=1)
```

plot hitmap type of infants body from body parts; TODO: will be added  



**Args:**
 
 - <b>`data`</b> (_type_):  _description_ 
 - <b>`x`</b> (_type_):  _description_ 
 - <b>`y`</b> (_type_):  _description_ 
 - <b>`locs`</b> (_type_):  _description_ 
 - <b>`images_path`</b> (_type_):  _description_ 
 - <b>`LOCATIONS`</b> (_type_):  _description_ 
 - <b>`max_val`</b> (int, optional):  _description_. Defaults to 1. 



**Returns:**
 
 - <b>`_type_`</b>:  _description_ 




---

_This file was automatically generated via [lazydocs](https://github.com/ml-tooling/lazydocs)._

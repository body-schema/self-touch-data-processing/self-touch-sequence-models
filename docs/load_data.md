<!-- markdownlint-disable -->

<a href="../infant_tools/load_data.py#L0"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

# <kbd>module</kbd> `load_data`




**Global Variables**
---------------
- **filename**
- **INFANTS**

---

<a href="../infant_tools/load_data.py#L10"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `get_all_data`

```python
get_all_data(data_path)
```

create a dictionary with structure same as database (time series representation)   {INFANT: {week: [recording1, recording2, ...]}}  values are pandas.DataFrame loaded time series 



**Args:**
 
 - <b>`data_path`</b> (str):  path to database folder (infant/week/recording/spatiotemporal.csv), files need to be names: spatiotemporal.csv 



**Returns:**
 
 - <b>`dict`</b>:  data_dict: loaded time series as pandas.DataFrame; filename_dict: same structure with recordings names 


---

<a href="../infant_tools/load_data.py#L48"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `save_data`

```python
save_data(data_path, data_dict, filename_dict)
```

saves data_dict with names from filename_dict to data_path 



**Args:**
 
 - <b>`data_path`</b> (str):  path where saved data will be stored 
 - <b>`data_dict`</b> (dict): data dictionary to save 
 - <b>`filename_dict`</b> (dict):  dictionary with recordings names 


---

<a href="../infant_tools/load_data.py#L69"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `get_video_length`

```python
get_video_length(video_file, framerate_file)
```

returns duration of recording in seconds 



**Args:**
 
 - <b>`video_file`</b> (str):  recording name 
 - <b>`framerate_file`</b> (str):  path to framerate.csv 



**Raises:**
 
 - <b>`FileNotFoundError`</b>:  if recording is not framerate.csv (recommend checking for typos, there used to be some in framerate.csv) 



**Returns:**
 
 - <b>`float`</b>:  recording duration 


---

<a href="../infant_tools/load_data.py#L89"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `get_video_framerate`

```python
get_video_framerate(video_file, framerate_file)
```

returns framerate of video recording  



**Args:**
 
 - <b>`video_file`</b> (str):  recording name 
 - <b>`framerate_file`</b> (str):  path to framerate.csv 



**Raises:**
 
 - <b>`FileNotFoundError`</b>:  if recording is not framerate.csv (recommend checking for typos, there used to be some in framerate.csv) 



**Returns:**
 
 - <b>`float`</b>:  framerate of recording 


---

<a href="../infant_tools/load_data.py#L110"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `apply_data_dict_function`

```python
apply_data_dict_function(data_dict, function, *args, **kwargs)
```

apply function to each value in data_dict   and returns a new data dict with the results   



**Args:**
 
 - <b>`data_dict`</b> (dict):  data in dictionary format (infant:week:[recordings]) 
 - <b>`function`</b> (Callable):  function to process values in data_dict with at least one argument 



**Returns:**
 
 - <b>`dict`</b>:  new data_dict with function values  


---

<a href="../infant_tools/load_data.py#L141"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `create_empty_dataset_dict`

```python
create_empty_dataset_dict(copy_dict)
```

creates empty dictionary with same structure as argument 



**Args:**
 
 - <b>`copy_dict`</b> (dict):  dictionary to copy 



**Returns:**
 
 - <b>`dict`</b>:  empty dictionary 


---

<a href="../infant_tools/load_data.py#L159"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `filter_data`

```python
filter_data(data_dict1, data_dict2, function, threshold, *args, **kwargs)
```

apply function to each value in data_dict1 and returns a new data_dict1 and data_dict2  with entries for which function output was >= than threshold  also returns dictionary with the output values (same structure as data_dict)   e.g., good for filtering short recordings         



**Args:**
 
 - <b>`data_dict1`</b> (dict):  data in dictionary format (infant:week:[recordings]), function is applied to this dictionary 
 - <b>`data_dict2`</b> (dict):  data in dictionary format (infant:week:[recordings]) 
 - <b>`function`</b> (Callable):  function to process values in data_dict1 with at least one argument 
 - <b>`threshold`</b> (dict):  only entries with function output >= threshold are kept  



**Returns:**
 
 - <b>`dict`</b>:  new_data_dict1: filtered data_dict1, new_data_dict2: data_dict2 with the same structure as data_dict1, new_val_dict: values from function 


---

<a href="../infant_tools/load_data.py#L198"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `apply_data_dict_function_all_weeks`

```python
apply_data_dict_function_all_weeks(data_dict, function, *args, **kwargs)
```

same as apply_data_dict_function, but the user function is applied to the whole week values  



**Args:**
 
 - <b>`data_dict`</b> (dict):  data in dictionary format (infant:week:[recordings]) 
 - <b>`function`</b> (Callable):  function to process week values in data_dict with at least one argument expecting list with values 



**Returns:**
 
 - <b>`dict`</b>:  dictionary with format {infant:week:[function output]} 


---

<a href="../infant_tools/load_data.py#L217"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `average_data_dict`

```python
average_data_dict(data_dict)
```

takes values from data_dict in infant's week and return average  



**Args:**
 
 - <b>`data_dict`</b> (dict):  data_dict in format data_dict[infant][week] = list 



**Returns:**
 
 - <b>`dict`</b>:  average dict for each week 


---

<a href="../infant_tools/load_data.py#L233"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `join_data_dict`

```python
join_data_dict(data_dict1, data_dict2)
```

joins two data dicts  



**Args:**
 
 - <b>`data_dict1`</b> (dict):  dict 1 
 - <b>`data_dict2`</b> (dict):  dict 2 



**Returns:**
 
 - <b>`dict`</b>: joined data_dict  




---

_This file was automatically generated via [lazydocs](https://github.com/ml-tooling/lazydocs)._

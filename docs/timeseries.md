<!-- markdownlint-disable -->

<a href="../infant_tools/timeseries.py#L0"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

# <kbd>module</kbd> `timeseries`




**Global Variables**
---------------
- **RH**
- **LH**

---

<a href="../infant_tools/timeseries.py#L12"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `get_actions_timeseries`

```python
get_actions_timeseries(timeseries)
```

creates array of actions in chronological order from timeseries  each touch or NO touch will be represented only by one entry in the actions series   e.g. [...,UPPER, UPPER, UPPER, NONE, NONE, NONE, HEAD HEAD, ...] -> [..., UPPER, NONE, HEAD, ...] 



**Args:**
 
 - <b>`timeseries`</b> (numpy.ndarray):  _description_ 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  actions time series 


---

<a href="../infant_tools/timeseries.py#L28"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `split_timeseries_by_help`

```python
split_timeseries_by_help(tm, help_symbol)
```

time series is split into parts 'before/after' help (during recording) 



**Args:**
 
 - <b>`tm`</b> (numpy.ndarray):  time series  
 - <b>`help_symbol`</b> (str):  label of help (default "HELP") in time series 



**Returns:**
 
 - <b>`list`</b>:  list of numpy.ndarrays as time series before, between, after help 


---

<a href="../infant_tools/timeseries.py#L43"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `back2normal`

```python
back2normal(tm)
```

converts 'back' areas in time series to front areas  e.g., 17LB to 17L  



**Args:**
 
 - <b>`tm`</b> (numpy.ndarray):  time series 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  time series with back areas converted to front areas 


---

<a href="../infant_tools/timeseries.py#L57"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `get_data_sequences`

```python
get_data_sequences(series, order=1)
```

creates list from moving window of size "order + 1"  e.g. [NONE, UPPER, NONE, LOWER] -> [[NONE, UPPER], [UPPER, NONE], [NONE, LOWER]] 





**Args:**
 
 - <b>`series`</b> (numpy.ndarray):  numerical time series or actions time series 
 - <b>`order`</b> (int):  size of moving window + 1. Defaults to 1. 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  list of all transitions  


---

<a href="../infant_tools/timeseries.py#L76"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `sliding_window`

```python
sliding_window(time_series, window_size, function, *args, **kwargs)
```

slide window of size window_size by one and apply function on every iteration  



**Args:**
 
 - <b>`time_series`</b> (numpy.ndarray):  - 
 - <b>`window_size`</b> (int):  size of sliding window  
 - <b>`function`</b> (Callable):  function to be applied on every window 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  results of function for every window step 


---

<a href="../infant_tools/timeseries.py#L94"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `count_pattern`

```python
count_pattern(time_series, seq)
```

count number of sequences in time series 



**Args:**
 
 - <b>`time_series`</b> (numpy.ndarray):  time series or series of actions ,... 
 - <b>`seq`</b> (list):  sequence to be counted 



**Returns:**
 
 - <b>`int`</b>:  number of sequences in data 


---

<a href="../infant_tools/timeseries.py#L112"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `merge_locations`

```python
merge_locations(time_series, locations_dict)
```

replace locations (states) in time series with other location defined by locations_dict,  could be used to merge multiple locations together (simplification of time series, cluster representation of t.m.) 



**Args:**
 
 - <b>`time_series`</b> (numpy.ndarray):  time series representation of qualitative data 
 - <b>`locations_dict`</b> (dict):  dictionary {cluster_1: [location_1, location_2, ...], cluster_2 : [...]...}  



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  time series where locations corresponding to clusters are replaced by cluster names 


---

<a href="../infant_tools/timeseries.py#L130"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `get_symbolic_timeseries`

```python
get_symbolic_timeseries(table)
```

creates symbolic representation of time series for each hand  each element of the time series represents a frame of the recording  and has its value is string representation of touched location (a columns names of the table)  if no touch took place, corresponding value is {hand}_NONE 



**Args:**
 
 - <b>`table`</b> (pandas DataFrame):  loaded spatiotemporal.csv 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  array symbolic time series for each hand 


---

<a href="../infant_tools/timeseries.py#L152"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `get_mapping`

```python
get_mapping(table)
```

for faster working with time series  string state values are mapped to numbers  should be generated only once for dataset 



**Args:**
 
 - <b>`table`</b> (pandas.DataFrame):  any loaded spatiotemporal.csv file from the set, we are working on 



**Returns:**
 
 - <b>`dict`</b>:  key is the symbolic (string) name of the state (as col. names in .csv), value is assigned number 


---

<a href="../infant_tools/timeseries.py#L169"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `get_timeseries`

```python
get_timeseries(table, hand, mapping)
```

get representation of timeseries coded to numbers  see functions get_symbolic_timeseries, get_mapping 



**Args:**
 
 - <b>`table`</b> (pandas.DataFrame):  loaded spatiotemporal.csv 
 - <b>`hand`</b> (string):  "RH" or "LH" 
 - <b>`mapping`</b> (dict):  result of func. get_mapping (only one per dataset) 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  time series where each element corresponds to frame and it's value is defined by mapping 


---

<a href="../infant_tools/timeseries.py#L188"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `num_tm2str_tm`

```python
num_tm2str_tm(timeseries, mapping)
```

convert time series (from get_timeseries) to symbolic / string representation (as get_symbolic_timeseries) 



**Args:**
 
 - <b>`timeseries`</b> (numpy.ndarray):  numerical time series, (each number represents state defined in mapping)  
 - <b>`mapping`</b> (dict):  result of func. get_mapping (only one per dataset) 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  symbolic representation of time series 


---

<a href="../infant_tools/timeseries.py#L206"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `remove_no_touch`

```python
remove_no_touch(timeseries, none_state)
```

removes NONE state (elements of time series when infant is not touching) 



**Args:**
 
 - <b>`timeseries`</b> (numpy.ndarray):  time series  
 - <b>`none_state`</b> (string):  value representing No Touch in time series 



**Returns:**
 
 - <b>`numpy.ndarray, dict`</b>:  time series without NONE touch state 


---

<a href="../infant_tools/timeseries.py#L219"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `get_actions_count`

```python
get_actions_count(timeseries, none_state)
```

returns an array containing the durations touches in time series  the i-th element of returned array corresponds to the duration (in frames) of the i-th action.  actions corresponds to the touch area from actions time series 



**Args:**
 
 - <b>`timeseries`</b> (numpy.ndarray):  time series  
 - <b>`none_state`</b> (string):  value representing No Touch in time series 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  count of frames per actions (touches) 


---

<a href="../infant_tools/timeseries.py#L244"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `get_pauses_between_touches`

```python
get_pauses_between_touches(timeseries, none_state)
```

returns an array containing the duration of no touch between touched areas.  the i-th element corresponds to the duration (in frames) of pause between i-th and (i+1)-th action.  actions corresponds to the touch area from actions time series.  the pause for complex touch is 0 



**Args:**
 
 - <b>`timeseries`</b> (numpy.ndarray):  time series  
 - <b>`none_state`</b> (string):  value representing No Touch in time series 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  count of no touch frames between actions (touches) 




---

_This file was automatically generated via [lazydocs](https://github.com/ml-tooling/lazydocs)._

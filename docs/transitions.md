<!-- markdownlint-disable -->

<a href="../infant_tools/transitions.py#L0"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

# <kbd>module</kbd> `transitions`






---

<a href="../infant_tools/transitions.py#L12"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>class</kbd> `Transitions`
object for counting transitions frequencies, conversion of frequency matrix to graph representation,  plotting of frequency matrix,  estimating markov model  

<a href="../infant_tools/transitions.py#L18"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `__init__`

```python
__init__(states, order=1)
```

Transitions class 



**Args:**
 
 - <b>`states`</b> (list):  list of states for model 
 - <b>`order`</b> (int, optional):  define order of classical markov model. Defaults to 1. 




---

<a href="../infant_tools/transitions.py#L106"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `count_transitions_in_range`

```python
count_transitions_in_range(
    tm,
    none_state,
    none_range,
    exclude_complex_touch=False
)
```

count frequency of transitions between touched areas (from time series)  only transitions between touched ares within `none_range` interval are included  



**Args:**
 
 - <b>`tm`</b> (numpy.ndarray):  time series  
 - <b>`none_state`</b> (str):  defined label of no touch state in time series 
 - <b>`none_range`</b> (list):  [min, max] range in number of frames  
 - <b>`exclude_complex_touch`</b> (bool, optional):  set to True if you want to count only transitions between simple touches  
 - <b>`(e.g., interval is [1 s, 10 s] and actions are`</b>:  '5L', 'NONE', '7L', '6L', 'NONE', '4L', 'NONE', '5L' then if duration of 'NONE' in in the interval the counted transitions are only 4L->5L; 
 - <b>`if set to False counted transitions are`</b>:  5L->7L, 6L->4L, 4L->5L). should be False when interval is [0s, ..s] to count transitions within complex touch. Defaults to False. 



**Raises:**
 
 - <b>`ValueError`</b>:  if this object is defined for order > 1 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  frequency matrix 

---

<a href="../infant_tools/transitions.py#L261"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `create_graph`

```python
create_graph(matrix=None, threshold=0, max_count=None)
```

creates a weighted directed graph representation of frequency matrix  using networkx library  



**Args:**
 
 - <b>`matrix`</b> (numpy.ndarray, optional):  matrix to normalize. Defaults to None - self.frequency_matrix is used. 
 - <b>`threshold`</b> (int, optional):  only transitions with weight (frequency) > threshold are included. Defaults to 0. 
 - <b>`max_count`</b> (int, optional):  include only the max_count number of most frequent transitions.   Defaults to None - all transitions are included. 



**Returns:**
 
 - <b>`networkx.classes.digraph.DiGraph`</b>:  weighted directed graph representation of frequency matrix 

---

<a href="../infant_tools/transitions.py#L296"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `draw_graph`

```python
draw_graph(
    ax,
    positions,
    DG=None,
    edge_cmap=<matplotlib.colors.LinearSegmentedColormap object at 0x7f765d0466b0>,
    **kwargs
)
```

draw directed graph on axis 



**Args:**
 
 - <b>`ax`</b> (matplotlib.axes.Axes):  axes to plot on 
 - <b>`positions`</b> (dict):  positions of labels  
 - <b>`DG`</b> (networkx.classes.digraph.DiGraph, optional):  Directed weighted graph to plot. Defaults to None - self.DG is used. 
 - <b>`edge_cmap`</b> (matplotlib.colors.Colormap, optional):  Colormap used for transitions. Defaults to plt.cm.Blues. 

---

<a href="../infant_tools/transitions.py#L75"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `fit_markov_model`

```python
fit_markov_model(x, y)
```

computes transition matrix from sequenced time series data,  works also with order > 1 



**Args:**
 
 - <b>`x`</b> (numpy.ndarray):  x from sequencer 
 - <b>`y`</b> (numpy.ndarray):  y from sequencer 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  transition matrix (value in row r and column c is the probability of transition from state r to state c)  order of these states is the same as in states when defining the model 

---

<a href="../infant_tools/transitions.py#L248"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `get_transition_table`

```python
get_transition_table()
```

returns transition table as pandas dataframe  useful for "visualization" of higher order models (use of multiindex)  



**Returns:**
 
 - <b>`pandas.DataFrame`</b>:  table representing transition probabilities between touched areas 

---

<a href="../infant_tools/transitions.py#L166"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `normalize`

```python
normalize(frequency_matrix=None, laplace_smoothing=False)
```

normalize rows of frequency matrix. the result is a transition matrix  with transition probabilities from row areas to column area   



**Args:**
 
 - <b>`frequency_matrix`</b> (numpy.ndarray, optional):  matrix to normalize. Defaults to None - self.frequency_matrix is used. 
 - <b>`laplace_smoothing`</b> (bool, optional):  apply laplace smoothing. Defaults to False. 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  transition matrix 

---

<a href="../infant_tools/transitions.py#L221"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `plot`

```python
plot(ax, cbar_ax=None)
```

plot frequency matrix on axis  only functional if order = 1 



**Args:**
 
 - <b>`ax`</b> (matplotlib axis):  axis to be plot on 
 - <b>`cbar_ax`</b> (matplotlib axis, optional):  axis for color bar. Defaults to None. 



**Returns:**
 
 - <b>`matplotlib axis`</b>:  axis with plotted transition matrix 

---

<a href="../infant_tools/transitions.py#L199"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `pruning`

```python
pruning(cutoff, frequency_matrix=None)
```

remove areas touched less then cutoff*100 %  



**Args:**
 
 - <b>`cutoff`</b> (int):  cutoff*100 % is the threshold  
 - <b>`frequency_matrix`</b> (numpy.ndarray, optional):  matrix to apply this function.   Defaults to None - self.frequency_matrix is used. 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  pruned frequency matrix  

---

<a href="../infant_tools/transitions.py#L41"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `sequencer`

```python
sequencer(tm)
```

generate sequences from time series for learning the model 



**Args:**
 
 - <b>`tm`</b> (numpy.ndarray):  time series (represented by sequence of states) 



**Returns:**
 
 - <b>`x,y (numpy.ndarray)`</b>:  x represents series of previous states whereas y represents the next states 

---

<a href="../infant_tools/transitions.py#L62"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `symbol_to_num`

```python
symbol_to_num(tm)
```

converts symbolic time series to numeric (for easier computation) 



**Args:**
 
 - <b>`tm`</b> (numpy.ndarray):  time series (represented by sequence of states) 



**Returns:**
 
 - <b>`numpy.ndarray`</b>:  converted time series 




---

_This file was automatically generated via [lazydocs](https://github.com/ml-tooling/lazydocs)._

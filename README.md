
# self-touch-sequence-models
Repository contains a small library **infant_tools** tailored for handling SELF_TOUCH_TIMESERIES dataset. The library offers additional tools for working with time series data. Two illustrative examples demonstrating the usage of the library are included.
  
  
# Data

Manually coded **qualitative data** have been transformed into a time series representation, a format used in this repository. Each row represents frame of video recording. Columns are: right hand, left hand and each value represents touched location in current frame. If no area was touched, the value is represented by **NONE** string constant. If there was an intervention of the caretaker during the recording, these frames are marked as **HELP**, together with touches that started during this period. The durations of touched areas within complex touches are uniformly distributed in time (frames).

> complex touches: transition between touched areas without loosing contact.

This repository includes a limited set of recordings stored in `data/SELF_TOUCH_TIMESERIES`, used in the provided examples.

# infant_tools

Small library was created with the following structure:

+  `load_data.py`: functions for loading data - entire database, and working with this structure
+  `timeseries.py`: functions for processing time series data
+  `transitions.py`: Transitions class for counting transitions between touched areas, visualizations, markov model estimation
+  `plotting.py`: a few functions for used for plotting

> Documentation in folder `docs/`. 

There are two examples:
+ `example_basics.ipynb` : loading the entire database, filtering recordings, counting number of touches and plotting, duration of touches, duration of pauses between touches
+ `example_transitions.ipynb`: counting transitions between touched areas (frequency matrices), figures with body scheme, example of density analysis

### installation
```bash
git clone https://gitlab.fel.cvut.cz/body-schema/self-touch-sequence-models.git
cd self-touch-sequence-models
pip install .
```

---
Juraj Marusic 2024
